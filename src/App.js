import React, { Component } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import './app.css';
import globo from './img/globo.png'
import axios from 'axios';
import { AwesomeButton } from 'react-awesome-button';
import 'react-awesome-button/dist/styles.css';
import Swal from 'sweetalert2'


class App extends Component {

  state = {
    tweet: '',
    arrayTweets: []
  }

  async getTweets() {
    return await axios.get(`http://localhost:7000/tweets`)
  }

  async componentDidMount() {
    try {
      const result = await this.getTweets()
      this.setState({ arrayTweets: result.data.tweets })
    } catch (e) {
      this._showPopOver('error', 'Não foi possivel requisitar os tweets, tente novamente.')
    }
  }

  _showPopOver = (type, msg) => {
    Swal({
      position: 'top-end',
      type: type,
      title: msg,
      showConfirmButton: false,
      timer: 2000
    })
  }

  async _tweetar() {
    try {
      await axios.post(`http://localhost:7000/tweet`, { tweet: this.state.tweet })
      this._showPopOver('success', 'Sucesso ao publicar o tweet.')
      this.setState({ tweet: '' })
      const result = await this.getTweets();
      if (result.data.tweets.length !== this.state.arrayTweets.length) {
        this.setState({ arrayTweets: result.data.tweets })
      }
    } catch (e) {
      this._showPopOver('error', 'Não foi possivel publicar, tente novamente.')
    }
  }

  render() {

    return (
      <div className='app-container'>
        <div className='box-tweet'>
          <div className='app-title'>
            Globo Chart
          </div>
          <div className='logo-app'>
            <img className='logo'
              src={globo} />
          </div>

          <div className='box-tweet'>
            <div className='app-title'>Make your vote</div>
            <div class="group">
              <input type="text" required id='input' value={this.state.tweet} onChange={(e) => this.setState({ tweet: e.target.value })} />
            </div>
            <div className='btn-tweet'>
              <AwesomeButton disabled={this.state.tweet.length === 0} action={() => this._tweetar()} type="primary">Tweetar</AwesomeButton>
            </div>
          </div>
          <BarChart width={500} height={300} data={this.state.arrayTweets}
            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis type="number" domain={[0, 23]} />/>
            <Tooltip />
            <Legend />
            <Bar dataKey="#MaisShampooSedoso" fill="#82ca9d" />
          </BarChart>
        </div>
      </div >
    );
  }
}

export default App;
